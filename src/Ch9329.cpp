#include "Ch9329.h"

// 接收发送处理函数
void Ch9329::task()
{
	Serial1.begin(9600, SERIAL_8N1, UART_RX, UART_TX);

	UART_FMT msg;
	while (1)
	{
		static BUFFER buf;
		static uint8_t n = 0;
		while (Serial1.available())
		{
			buf.data[n] = Serial1.read();
			n++;
		}
		if (n > 0)
		{
			buf.len = n;
			n = 0;
			analyzeCommand(buf);
		}
		vTaskDelay(1);

		if (xQueueReceive(queueMsg, &msg, 20) == pdPASS)
		{
			Serial1.write(frameTObuf(msg).data, frameTObuf(msg).len); // 发送指令
			vTaskDelay(20);
			ESP_LOGD("SEND", "send CMD 0x%02x success", msg.CMD);
		}
	}
}

///  @brief 构造函数
Ch9329::Ch9329() { xTaskCreate(this->startTaskImpl, "B", 1024 * 4, this, 1, NULL); }

/******************************************************************************************************
 * @brief 发送命令
 *
 * @param command 指令码
 * @param length 数据长度
 * @param length 数据内容
 *****************************************************************************************************/
void Ch9329::sendCommand(char command, uint8_t length, char *buf)
{

	UART_FMT fmt;
	fmt.HEAD[0] = FRAME_HEADER_1;
	fmt.HEAD[1] = FRAME_HEADER_2;
	fmt.ADDR = CHIP_ADDR;
	fmt.CMD = command;
	fmt.LEN = length;
	for (int i = 0; i < length; i++)
		fmt.DATA[i] = *(buf + i);

	setSum(&fmt);
	xQueueSend(queueMsg, &fmt, 100);
}

/******************************************************************************************************
 * @brief 发送测试命令
 *
 * @param command 指令码
 * @param length 数据长度+数据内容
 *****************************************************************************************************/
void Ch9329::test_command(char command, char length, ...)
{
	char buf[length];
	va_list args;
	va_start(args, length); // args指向栈顶
	for (int i = 0; i < length; ++i)
		buf[i] = va_arg(args, int);
	va_end(args);

	UART_FMT fmt;
	fmt.HEAD[0] = FRAME_HEADER_1;
	fmt.HEAD[1] = FRAME_HEADER_2;
	fmt.ADDR = CHIP_ADDR;
	fmt.CMD = command;
	fmt.LEN = length;
	strcpy(fmt.DATA, buf);
	setSum(&fmt);
	xQueueSend(queueMsg, &fmt, 100);
}

/******************************************************************************************************
 * @brief
 *
 * @param t_work_mode 工作模式
 * @param t_uart_mode 串口模式
 * @param t_baudrate 波特率
 * @param t_vid 厂家ID
 * @param t_pid 产品ID
 *****************************************************************************************************/
void Ch9329::set_configure(WORK_MODE t_work_mode, UART_MODE t_uart_mode, int32_t t_baudrate, int32_t t_vid, int32_t t_pid)
{
	work_mode = t_work_mode;
	uart_mode = t_uart_mode;
	baudrate = t_baudrate;
	vid = t_vid;
	pid = t_pid;

	char buf[50];
	configTobuf(buf);
	sendCommand(CMD_SET_PARA_CFG, 50, buf);

	vTaskDelay(10);
	reset(); // 需要重启后生效
}

/// @brief 数据分析函数
void Ch9329::analyzeCommand(BUFFER buf)
{
	int len = buf.data[4];
	// 判断帧头是否是设定值, 还有校验和是否正确
	if (buf.data[0] != FRAME_HEADER_1 || buf.data[1] != FRAME_HEADER_2 || len != (buf.len - 6))
		return;

	UART_FMT fmt = bufTOframe(buf);

	ESP_LOGD("ANALYZE", "received CMD 0x%x", fmt.CMD);

	if ((fmt.CMD & 0xF0) == 0x80)
	{
		switch (fmt.CMD)
		{
		case CMD_GET_INFO | 0x80: // 获取芯片版本等信息
		{
			version = (0x01 << 8) | (0x0ffff) & (fmt.DATA[0] & 0xff0f);

			// 灯状态改变
			if (getBit(fmt.DATA[2], 0))
				num_lock_light = true;
			else
				num_lock_light = false;

			if (getBit(fmt.DATA[2], 1))
				caps_lock_light = true;
			else
				caps_lock_light = false;

			if (getBit(fmt.DATA[2], 2))
				scroll_lock_light = true;
			else
				scroll_lock_light = false;

			if (_USB_CONNECT_FUNC != NULL)
				_USB_CONNECT_FUNC();

			ESP_LOGD("CMD_GET_INFO", "version: V%d.%d  num_lock[%d] caps_lock[%d] scroll_lock[%d] \n",
					 version >> 8, version & 0x00ff, num_lock_light, caps_lock_light, scroll_lock_light);
		}
		break;
		case CMD_GET_PARA_CFG | 0x80: // 获取参数配置
		{
			bufToconfig(fmt.DATA);
			ESP_LOGD("CMD_GET_PARA_CFG", "Work Mode[0x%02x] Uart Mode[0x%02x] Address[0x%02x] Baudratr[0x%zu] Packet interval[%dms] VID[0x%x] PID[0x%x] ",
					 work_mode, uart_mode, addr, baudrate, packet_interval, vid, pid);
		}
		break;
		case CMD_GET_USB_STRING | 0x80: // 获取字符串描述符配置
		{
			char str[fmt.DATA[1]];
			for (int8_t i = 0; i < fmt.DATA[1]; i++)
				str[i] = fmt.DATA[i + 2];
			str[fmt.DATA[1]] = '\0';

			if (fmt.DATA[0] == 0x00)
				str_manufacturer = str;
			else if (fmt.DATA[0] == 0x01)
				str_product = str;
			else if (fmt.DATA[0] == 0x02)
				str_serial = str;

			ESP_LOGD("CMD_GET_USB_STRING", "Type[%02x] Value[%s]", fmt.DATA[0], str);
		}
		break;
		case CMD_READ_MY_HID_DATA:
		{
			for (int8_t i = 0; i < fmt.LEN; i++)
				ESP_LOGD("HID", "%02x", fmt.DATA[i]);

			if (_GET_HID_REPORT_FUNC != NULL)
				_GET_HID_REPORT_FUNC(fmt.DATA, fmt.LEN);
		}
		break;
		default:
			break;
		}
	}
	else if ((fmt.CMD & 0xF0) == 0xC0)
	{
		RESULTS res;
		res.cmd = fmt.CMD;
		res.error = (ERROR)fmt.DATA[0];

		ESP_LOGD("ERROR", "CMD[%02x] Value[%s]", fmt.CMD, fmt.DATA[0]);

		if (_GET_RESULTS_FUNC != NULL)
			_GET_RESULTS_FUNC(res);
	}
}

/*****************************************************************************************************
 * @brief 把 int32_t 转化为波特率 byte 数组
 *
 * @param data 数据
 * @param s	数组指针
 * @param offset 数组指针偏移量, 有时候并不想把转换结果放在数组第一个位置
 *****************************************************************************************************/
void Ch9329::int32ToBaudrate(int32_t data, char *s, int offset)
{
	*(s + 3 + offset) = data;
	*(s + 2 + offset) = data >> 8;
	*(s + 1 + offset) = data >> 16;
	*(s + offset) = data >> 24;
}
/// @brief 把波特率 byte 数组转化为 int32_t
int32_t Ch9329::BaudrateToint32(char *data)
{
	int32_t baud = *(data + 3) & 0xFF;
	baud |= ((*(data + 2) << 8) & 0xFF00);
	baud |= ((*(data + 1) << 16) & 0xFF0000);
	baud |= ((*(data + 0) << 24) & 0xFF000000);
	return baud;
}
/// @brief 把指令帧结构体转化为 byte 数组
BUFFER Ch9329::frameTObuf(UART_FMT data)
{
	BUFFER buf;
	buf.len = data.LEN + 6;
	buf.data[0] = data.HEAD[0];
	buf.data[1] = data.HEAD[1];
	buf.data[2] = data.ADDR;
	buf.data[3] = data.CMD;
	buf.data[4] = data.LEN;
	for (int i = 0; i < data.LEN; i++)
	{
		buf.data[5 + i] = data.DATA[i];
	}
	buf.data[5 + data.LEN] = data.SUM;
	return buf;
}
/// @brief 把数组转化为配置结构体
UART_FMT Ch9329::bufTOframe(BUFFER buf)
{
	UART_FMT fmt;
	fmt.HEAD[0] = buf.data[0];
	fmt.HEAD[1] = buf.data[1];
	fmt.ADDR = buf.data[2];
	fmt.CMD = buf.data[3];
	fmt.LEN = buf.data[4];
	for (int i = 0; i < fmt.LEN; i++)
	{
		fmt.DATA[i] = buf.data[5 + i];
	}
	fmt.SUM = buf.data[buf.len - 1];
	return fmt;
}
/// @brief 获取字节指定位置的值
bool Ch9329::getBit(char data, int position) { return data & (1 << position); }
/// @brief 计算指令帧结构体校验和
void Ch9329::setSum(UART_FMT *data)
{
	char t = data->HEAD[0] + data->HEAD[1] + data->ADDR + data->CMD + data->LEN;
	for (int i = 0; i < data->LEN; i++)
		t += data->DATA[i];
	data->SUM = t & 0xFF;
}
/// @brief 把芯片配置信息转化为数组
void Ch9329::configTobuf(char *s)
{
	// 芯片工作模式
	*(s + 0) = work_mode;
	// 串口通信模式
	*(s + 1) = uart_mode;
	// 芯片串口通信地址
	*(s + 2) = addr;
	// 串口通信波特率
	int32ToBaudrate(baudrate, s, 3);
	// 2个字节保留
	*(s + 7) = 0x08;
	*(s + 8) = 0x00;
	//  2个字节芯片串口通信包间隔
	*(s + 9) = packet_interval >> 8;
	*(s + 10) = packet_interval;
	// 芯片USB的VID和PID
	*(s + 11) = vid;
	*(s + 12) = vid >> 8;
	*(s + 13) = pid;
	*(s + 14) = pid >> 8;
	// 2个字节芯片USB键盘上传时间间隔
	*(s + 15) = kb_updata_interval >> 8;
	*(s + 16) = kb_updata_interval;
	// 2个字节芯片USB键盘释放延时时间
	*(s + 17) = kb_reless_interval >> 8;
	*(s + 18) = kb_reless_interval;
	// 1个字节芯片USB键盘自动回车标志
	if (kb_autoEnter)
		*(s + 19) = 0x01;
	else
		*(s + 19) = 0x00;
	// 8个字节芯片USB键盘回车符
	for (int i = 0; i < 4; i++)
		*(s + 20 + i) = enter_1[i];
	for (int i = 0; i < 4; i++)
		*(s + 24 + i) = enter_2[i];
	// 8个字节芯片USB键盘过滤开始、结束字符串
	for (int i = 0; i < 8; i++)
		*(s + 32 + i) = filter_str[i];
	// 1个字节芯片USB字符串使能标志
	int8_t usb_str_en = 0x00;
	if (en_custom_str)
		usb_str_en |= 0x80;
	else
		usb_str_en &= 0x7F;
	if (en_v_str)
		usb_str_en |= 0x04;
	else
		usb_str_en &= 0xFC;
	if (en_p_str)
		usb_str_en |= 0x02;
	else
		usb_str_en &= 0xFD;
	if (en_s_str)
		usb_str_en |= 0x01;
	else
		usb_str_en &= 0xFE;
	*(s + 33) = usb_str_en;
	// 1个字节芯片USB键盘快速上传标
	if (kb_fast_update)
		*(s + 34) = 0x01;
	else
		*(s + 34) = 0x00;
}
/// @brief 把数组转化为配置结构体
void Ch9329::bufToconfig(char data[])
{
	work_mode = (WORK_MODE)data[0];
	uart_mode = (UART_MODE)data[1];
	addr = data[2];

	baudrate = BaudrateToint32(&data[3]);

	int16_t pi = data[10] & 0xFF;
	pi |= ((data[9] << 8) & 0xFF00);
	packet_interval = pi;

	int32_t vid = data[11] & 0xFF;
	vid |= ((data[12] << 8) & 0xFF00);
	vid = vid;

	int32_t pid = data[13] & 0xFF;
	pid |= ((data[14] << 8) & 0xFF00);
	pid = pid & 0x0000FFFF;
	pid = pid;

	int16_t kui = data[15] & 0xFF;
	kui |= ((data[16] << 8) & 0xFF00);
	kb_updata_interval = kui;

	int16_t kri = data[17] & 0xFF;
	kri |= ((data[18] << 8) & 0xFF00);
	kb_reless_interval = kri;

	if (data[19] = 0x00)
		kb_autoEnter = false;
	else if (data[19] = 0x01)
		kb_autoEnter = true;

	for (size_t i = 0; i < 4; i++)
		enter_1[i] = data[20 + i];

	for (size_t i = 0; i < 4; i++)
		enter_2[i] = data[24 + i];

	for (size_t i = 0; i < 8; i++)
		filter_str[i] = data[28 + i];

	en_custom_str = getBit(data[36], 7);
	en_v_str = getBit(data[36], 2);
	en_p_str = getBit(data[36], 1);
	en_s_str = getBit(data[36], 0);

	if (data[37] == 0x00)
		kb_fast_update = false;
	else if (data[37] == 0x01)
		kb_fast_update = true;
}
///  @brief 获取芯片配置
void Ch9329::get_configure() { sendCommand(CMD_GET_PARA_CFG); }
///  @brief 获取状态
void Ch9329::get_info() { sendCommand(CMD_GET_INFO); }
///  @brief 重启芯片
void Ch9329::reset() { sendCommand(CMD_RESET); }
///  @brief 恢复出厂设置
void Ch9329::factory_data_reset() { sendCommand(CMD_SET_DEFAULT_CFG); }
///  @brief 恢复出厂设置
void Ch9329::get_usb_manufacturer_string()
{
	char buf[1] = {0x00};
	sendCommand(CMD_GET_USB_STRING, 1, buf);
}
///  @brief 获取产品描述
void Ch9329::get_usb_product_string()
{
	char buf[1] = {0x01};
	sendCommand(CMD_GET_USB_STRING, 1, buf);
}
///  @brief 获取序列号描述
void Ch9329::get_usb_serial_string()
{
	char buf[1] = {0x02};
	sendCommand(CMD_GET_USB_STRING, 1, buf);
}
///  @brief 设置厂商描述符
void Ch9329::set_usb_manufacturer_string(String str)
{
	char buf[str.length() + 2];
	buf[0] = 0x00;
	buf[1] = str.length();
	for (int i = 0; i < str.length(); i++)
		buf[2 + i] = str[i];

	sendCommand(CMD_SET_USB_STRING, str.length() + 2, buf);
}
///  @brief 设置产品描述符
void Ch9329::set_usb_product_string(String str)
{
	char buf[str.length() + 2];
	buf[0] = 0x01;
	buf[1] = str.length();
	for (int i = 0; i < str.length(); i++)
		buf[2 + i] = str[i];

	sendCommand(CMD_SET_USB_STRING, str.length() + 2, buf);
}
///  @brief 设置序列号描述符
void Ch9329::set_usb_serial_string(String str)
{
	char buf[str.length() + 2];
	buf[0] = 0x02;
	buf[1] = str.length();
	for (int i = 0; i < str.length(); i++)
		buf[2 + i] = str[i];

	sendCommand(CMD_SET_USB_STRING, str.length() + 2, buf);
}
///  @brief 发送自定义 HID 数据包
void Ch9329::set_hid_report(char *buff, uint8_t length)
{
	sendCommand(CMD_SEND_MY_HID_DATA, length, buff);
}