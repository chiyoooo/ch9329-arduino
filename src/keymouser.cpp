#include <keymouser.h>

/*****************************************************************************************************
 * @brief 判断key 键值是否在已经按下的数组中
 *
 * @param key 新键值
 * @return int8_t   大于等于 0 表示该键值在数组中的位置
 *                  -1 该键值是新键值
 */
int8_t Keymouser ::inAarry(const byte key)
{
    // 如果在列表中就返回在列表中的位置
    for (int i = 0; i < MAX_PRESS_COUNT; i++)
        if (key == now_press_key[i])
            return i;
    // 表示该键值是一个新键值
    return -1;
}

/*****************************************************************************************************
 * @brief 判断按下的键值是辅助键还是普通键，并将键值放入 press_modify_key 或 now_press_key[MAX_PRESS_COUNT] 中
 *
 * @param key 新键值
 */
void Keymouser ::press_one_key(const byte key)
{
    if (ISMODIFYKEY(key))
    {
        press_modify_key |= key;
    }
    else if (key < 0x80 && now_press_key_count < MAX_PRESS_COUNT)
    {
        int8_t index = inAarry(key);
        // 新键值 避免重复输入同一个键值
        if (index == -1)
        {
            now_press_key[now_press_key_count] = key;
            now_press_key_count++;
        }
    }
}

/*****************************************************************************************************
 * @brief 判断释放的键值是辅助键还是普通键，并从 press_modify_key 或 now_press_key[MAX_PRESS_COUNT] 中删除相应键值
 *
 * @param key
 */
void Keymouser ::release_one_key(const byte key)
{
    // 判断是否是 辅助键值
    if (ISMODIFYKEY(key))
        press_modify_key &= ~key;
    else if (key < 0x80)
    {
        int8_t index = inAarry(key);
        if (index >= 0)
        {
            now_press_key[index] = 0x00;
            now_press_key_count--;
            for (int i = index; i < MAX_PRESS_COUNT; i++)
                if (now_press_key[i] == 0x00 && i < 5)
                {
                    now_press_key[i] = now_press_key[i + 1];
                    now_press_key[i + 1] = 0x00;
                }
            now_press_key[5] = 0x00;
        }
    }
}

/*****************************************************************************************************
 * @brief 发送键盘键值数据
 *
 * @param modifyKey 辅助键键值 WIN SHIFT CTRL ALT
 * @param keyArry 普通键键值数组
 */
void Keymouser ::send_keyboard_data(byte modifyKey, byte keyArry[])
{
    UART_FMT fmt;
    fmt.HEAD[0] = FRAME_HEADER_1;
    fmt.HEAD[1] = FRAME_HEADER_2;
    fmt.ADDR = 0x00;
    fmt.CMD = CMD_SEND_KB_GENERAL_DATA;
    fmt.LEN = 0x08;

    fmt.DATA[0] = modifyKey;                  // 控制键，每个位表示 1 个按键
    fmt.DATA[1] = 0x00;                       // 该字节必须为 0x00
    for (int i = 0; i < MAX_PRESS_COUNT; i++) // 6 个字节普通按键值，最多可以表示 6 个按键按下
        fmt.DATA[2 + i] = keyArry[i];         // 如果无按键按下则填写 0x00

    setSum(&fmt);
    xQueueSend(queueMsg, &fmt, 100);

    // ESP_LOGD("KEYBOARD", "Modify Key: 0x%02x  COUNT: %d [0x%02x 0x%02x 0x%02x 0x%02x 0x%02x 0x%02x] \n",
    //          modifyKey,
    //          now_press_key_count,
    //          keyArry[0], keyArry[1], keyArry[2], keyArry[3], keyArry[4], keyArry[5]);
}

/*****************************************************************************************************
 * @brief 发送相对鼠标数据包
 *
 * @param key 鼠标左中右键键值
 * @param x 光标移动距离x
 * @param y 光标移动距离y
 * @param scroll 滚轮滚动格数
 */
void Keymouser ::send_rel_mouse_data(byte key, int8_t x, int8_t y, int8_t scroll) // 相对鼠标数据包
{
    UART_FMT fmt;
    fmt.HEAD[0] = FRAME_HEADER_1;
    fmt.HEAD[1] = FRAME_HEADER_2;
    fmt.ADDR = 0x00;
    fmt.CMD = CMD_SEND_MS_REL_DATA;
    fmt.LEN = 0x05;

    fmt.DATA[0] = 0x01;
    fmt.DATA[1] = key; // 1个字节的鼠标按键值，最低3位每位表示1个按键
    fmt.DATA[2] = x;
    fmt.DATA[3] = y;
    fmt.DATA[4] = scroll;

    setSum(&fmt);
    xQueueSend(queueMsg, &fmt, 100);
}

/*****************************************************************************************************
 * @brief 发送绝对鼠标数据包
 *
 * @param key 鼠标左中右键键值
 * @param x 光标移动距离x 低字节在前，高字节在后
 * @param y 光标移动距离y 低字节在前，高字节在后
 * @param scroll 滚轮滚动格数
 */
void Keymouser ::send_abs_mouse_data(byte key, int16_t x, int16_t y, int8_t scroll) // 相对鼠标数据包
{

    UART_FMT fmt;
    fmt.HEAD[0] = FRAME_HEADER_1;
    fmt.HEAD[1] = FRAME_HEADER_2;
    fmt.ADDR = 0x00;
    fmt.CMD = CMD_SEND_MS_ABS_DATA;
    fmt.LEN = 0x07;

    fmt.DATA[0] = 0x02;
    fmt.DATA[1] = key; // 1个字节的鼠标按键值，最低3位每位表示1个按键

    x = (4096 * x) / SCREEN_WIDTH;
    fmt.DATA[2] = x;
    fmt.DATA[3] = x >> 8;

    y = (4096 * y) / SCREEN_HIGH;
    fmt.DATA[4] = y;
    fmt.DATA[5] = y >> 8;

    fmt.DATA[6] = scroll;

    setSum(&fmt);
    xQueueSend(queueMsg, &fmt, 100);
}

/*****************************************************************************************************
 * @brief 键盘按下
 *
 * @param key ... 键值列表，最多用6个普通键值，多出将自动忽略
 */
void Keymouser ::press(KeyboardKey key, ...)
{
    va_list pp;
    va_start(pp, key);
    do
    {
        press_one_key(key);
        key = va_arg(pp, int); //使pp指向下一个参数,将下一个参数的值赋给变量key
    } while (key != END);
    va_end(pp); //将pp的值置为NULL
    send_keyboard_data(press_modify_key, now_press_key);
}

/*****************************************************************************************************
 * @brief 键盘释放
 *
 * @param key ... 键值列表，最多用6个普通键值，多出将自动忽略
 */
void Keymouser ::release(KeyboardKey key, ...)
{
    va_list pp;
    va_start(pp, key);
    do
    {
        release_one_key(key);
        key = va_arg(pp, int); //使pp指向下一个参数,将下一个参数的值赋给变量key
    } while (key != END);
    va_end(pp); //将pp的值置为NULL
    send_keyboard_data(press_modify_key, now_press_key);
}

/*****************************************************************************************************
 * @brief 鼠标按下
 *
 * @param key ... 键值列表
 */
void Keymouser ::press(MouseKey key, ...)
{
    va_list pp;
    va_start(pp, key);
    do
    {
        if (ISMOUSEKEY(key))
            press_mouse_key |= key;
        key = va_arg(pp, int); //使pp指向下一个参数,将下一个参数的值赋给变量key
    } while (key != END);
    va_end(pp); //将pp的值置为NULL

    send_rel_mouse_data(press_mouse_key, 0, 0, 0);
}

/*****************************************************************************************************
 * @brief 鼠标释放
 *
 * @param key ... 键值列表
 */
void Keymouser ::release(MouseKey key, ...)
{
    va_list pp;
    va_start(pp, key);
    do
    {
        if (ISMOUSEKEY(key))
            press_mouse_key &= ~key;
        key = va_arg(pp, int); //使pp指向下一个参数,将下一个参数的值赋给变量key
    } while (key != END);
    va_end(pp); //将pp的值置为NULL

    send_rel_mouse_data(press_mouse_key, 0, 0, 0);
}

/*****************************************************************************************************
 * @brief 键盘单击
 *
 * @param key ... 键值列表，最多用6个普通键值，多出将自动忽略
 */
void Keymouser ::click(KeyboardKey key, ...)
{
    byte t_now_press_key[6] = {
        0x00,
        0x00,
        0x00,
        0x00,
        0x00,
        0x00};
    byte t_press_modify_key;
    uint8_t n = 0;
    va_list pp;
    va_start(pp, key);
    do
    {
        if (ISMODIFYKEY(key))
            t_press_modify_key |= key;
        else if (key < 0x80)
        {
            int8_t index = inAarry(key);
            if (index == -1)
            {
                t_now_press_key[n] = key;
                n++;
            }
        }
        key = va_arg(pp, int); //使pp指向下一个参数,将下一个参数的值赋给变量key
    } while (key != END);
    va_end(pp); //将pp的值置为NULL

    send_keyboard_data(t_press_modify_key, t_now_press_key);

    t_press_modify_key = 0x00;
    for (int i = 0; i < MAX_PRESS_COUNT; i++)
        t_now_press_key[i] = 0x00;
    send_keyboard_data(t_press_modify_key, t_now_press_key);
}

/*****************************************************************************************************
 * @brief 鼠标单击
 *
 * @param key ... 键值列表
 */
void Keymouser ::click(MouseKey key, ...)
{
    byte t_press_mouse_key;

    va_list pp;
    va_start(pp, key);
    do
    {
        if (ISMOUSEKEY(key))
            t_press_mouse_key |= key;

        key = va_arg(pp, int); //使pp指向下一个参数,将下一个参数的值赋给变量key
    } while (key != END);
    va_end(pp); //将pp的值置为NULL

    send_rel_mouse_data(t_press_mouse_key, 0, 0, 0);
    t_press_mouse_key = 0x00;
    send_rel_mouse_data(t_press_mouse_key, 0, 0, 0);
}

/*****************************************************************************************************
 * @brief 多媒体键单击
 *
 * @param key  键值
 */
void Keymouser ::click(const MediaKey key)
{
    UART_FMT fmt;
    fmt.HEAD[0] = FRAME_HEADER_1;
    fmt.HEAD[1] = FRAME_HEADER_2;
    fmt.ADDR = 0x00;
    fmt.CMD = CMD_SEND_KB_MEDIA_DATA;
    fmt.LEN = 0x04;

    for (int i = 0; i < 4; i++)
        fmt.DATA[i] = *(key + i);

    setSum(&fmt);
    xQueueSend(queueMsg, &fmt, 100);

    vTaskDelay(50);
    for (int i = 0; i < 4; i++)
        fmt.DATA[i] = 0x00;

    setSum(&fmt);
    xQueueSend(queueMsg, &fmt, 100);
}

/*****************************************************************************************************
 * @brief 鼠标滚动
 *
 * @param d  滚动格数 1~127 向上滚动   -1~-127 向下滚动
 */
void Keymouser ::scroll(int8_t d) { send_rel_mouse_data(0x00, 0, 0, d); }

/*****************************************************************************************************
 * @brief 光标移动
 *
 * @param x 光标移动距离x 1~127 向右移动   -1~-127 向左移动
 * @param y 光标移动距离y 1~127 向右移动   -1~-127 向左移动
 */
void Keymouser ::moveby(int8_t x, int8_t y) { send_rel_mouse_data(0x00, x, y, 0); }

/*****************************************************************************************************
 * @brief 光标移动到
 *
 * @param x 光标移动距离x 1~32767 向右移动   -1~-32767 向左移动
 * @param y 光标移动距离y 1~32767 向右移动   -1~-32767 向左移动
 */
void Keymouser ::moveto(int16_t x, int16_t y) { send_abs_mouse_data(0x00, x, y, 0); }

/*****************************************************************************************************
 * @brief 光标开始拖拽
 *
 * @param k 左键/中键/右键 拖拽
 * @param x 拖拽距离x 1~127 向右移动   -1~-127 向左移动
 * @param y 拖拽距离y 1~127 向右移动   -1~-127 向左移动
 */
void Keymouser ::start_drag(MouseKey k, int8_t x, int8_t y) { send_rel_mouse_data(k, x, y, 0); }

/**
 * @brief 光标结束拖拽
 *
 * @param k 释放的键值
 */
void Keymouser ::stop_drag(MouseKey k) { send_rel_mouse_data(0, 0, 0, 0); }

void Keymouser ::printstr(String str)
{
    for (int i = 0; i < str.length(); i++)
    {
        if ((str.charAt(i) - ASCIIOFFSET) != 0)
        {
            KeyboardKey key = HIDCONVERTASCII[str.charAt(i) - ASCIIOFFSET];
            KeyboardKey modify = HIDCONVERTASCII[str.charAt(i) - ASCIIOFFSET] >> 8;
            click(key, modify, END);
        }
        else
            click(KEY_SPACEBAR, END);
    }
}