#pragma once

#include "Arduino.h"
#include "Ch9329.h"
#include "KeyCode.h"

#define SCREEN_WIDTH 3440
#define SCREEN_HIGH 1440
#define MAX_PRESS_COUNT 6

class Keymouser : public Ch9329
{

private:
    uint16_t screen_width;               // 屏幕宽度，用于计算绝对鼠标坐标值
    uint16_t screen_high;                // 屏幕高度，用于计算绝对鼠标坐标值
    uint8_t now_press_key_count = 0;     // 当前按住的键的数量，ch9329 只支持同时按下6个普通按键
    byte now_press_key[MAX_PRESS_COUNT]; // 当前按住的键的键值
    byte press_modify_key;               // 当前按下的辅助键 WIN SHIFT CTRL ALT 四个键值
    byte press_mouse_key;                // 当前按下的鼠标键键值 LEFT RIGHT MIDDLE 三个键值

protected:
    int8_t inAarry(const byte key);                                          // 判断key 键值是否在已经按下的数组中
    void press_one_key(const byte key);                                      // 判断按下的键值是辅助键还是普通键
    void release_one_key(const byte key);                                    // 判断释放的键值是辅助键还是普通键
    void send_keyboard_data(ModifyKey mk, KeyboardKey keyArry[]);            // 发送键盘键值数据
    void send_rel_mouse_data(byte key, int8_t x, int8_t y, int8_t scroll);   // 发送相对鼠标数据包
    void send_abs_mouse_data(byte key, int16_t x, int16_t y, int8_t scroll); // 发送绝对鼠标数据包

public:
    void press(KeyboardKey key, ...);   // 键盘按下
    void release(KeyboardKey key, ...); // 键盘释放

    void press(MouseKey key, ...);   // 鼠标按下
    void release(MouseKey key, ...); // 鼠标释放

    void click(KeyboardKey key, ...); // 键盘单击
    void click(MouseKey key, ...);    // 鼠标单击
    void click(const MediaKey key);   // 多媒体键单击

    void scroll(int8_t d);                           // 鼠标滚动
    void moveby(int8_t x, int8_t y);                 // 光标移动
    void moveto(int16_t x, int16_t y);               // 光标移动到
    void start_drag(MouseKey k, int8_t x, int8_t y); // 光标开始拖拽
    void stop_drag(MouseKey k);                      // 光标结束拖拽

    void printstr(String str); // 打印字符串
};
