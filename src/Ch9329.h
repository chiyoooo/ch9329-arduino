#ifndef _CH9329_H_
#define _CH9329_H_
#pragma once
#include "Arduino.h"

#define FRAME_HEADER_1 0x57 // ֡ͷ
#define FRAME_HEADER_2 0xAB // ֡ͷ
#define CHIP_ADDR 0x00		// Ĭ�ϵ�ַ��
#define UART_TX 11			// MCU ���� TX�ţ�������CH9329 ��RX��
#define UART_RX 7			// MCU ���� RX�ţ�������CH9329 ��TX��
/*ָ����*/
#define CMD_GET_INFO 0x01			  // ��ȡоƬ��ȡ�汾�š� USB ö��״̬�����̴�Сдָʾ��״̬����Ϣ
#define CMD_SEND_KB_GENERAL_DATA 0x02 // ���� USB ������ͨ����
#define CMD_SEND_KB_MEDIA_DATA 0x03	  // ���� USB ���̶�ý������
#define CMD_SEND_MS_ABS_DATA 0x04	  // ���� USB �����������
#define CMD_SEND_MS_REL_DATA 0x05	  // ���� USB ����������
#define CMD_SEND_MY_HID_DATA 0x06	  // ���� USB �Զ�������
#define CMD_READ_MY_HID_DATA 0x87	  // ��ȡ USB �Զ�������
#define CMD_GET_PARA_CFG 0x08		  // ��ȡоƬ��������
#define CMD_SET_PARA_CFG 0x09		  // ����оƬ��������
#define CMD_GET_USB_STRING 0x0A		  // ��ȡ�ַ�������������
#define CMD_SET_USB_STRING 0x0B		  // �����ַ�������������
#define CMD_SET_DEFAULT_CFG 0x0C	  // �ָ�����Ĭ������
#define CMD_RESET 0x0F				  // ��λоƬ

/* оƬ����ģʽ */
enum WORK_MODE
{
	SW_WORK_MODE_0 = 0x00, // �������õĹ���ģʽ0����׼USB����(��ͨ+��ý��)+USB���(�������+������)
	SW_WORK_MODE_1 = 0x01, // �������õĹ���ģʽ1����׼USB����(��ͨ)
	SW_WORK_MODE_2 = 0x02, // �������õĹ���ģʽ2����׼USB���(�������+������)
	SW_WORK_MODE_3 = 0x03, // �������õĹ���ģʽ3����׼USB�Զ���HID���豸

	HW_WORK_MODE_0 = 0x80, // Ӳ���������õĹ���ģʽ0����׼USB����(��ͨ+��ý��)+USB���(�������+������) M0 = 1, M1 = 1
	HW_WORK_MODE_1 = 0x81, // Ӳ���������õĹ���ģʽ1����׼USB����(��ͨ) M0 = 0, M1 = 1
	HW_WORK_MODE_2 = 0x82, // Ӳ���������õĹ���ģʽ2����׼USB���(�������+������) M0 = 1, M1 = 0
	HW_WORK_MODE_3 = 0x83, // Ӳ���������õĹ���ģʽ3����׼USB�Զ���HID���豸 M0 = 0, M1 = 0
};

/* ����ͨ��ģʽ */
enum UART_MODE
{
	SW_UART_MODE_0 = 0x00, // �������õĴ���ͨ��ģʽ0��Э�鴫��ģʽ
	SW_UART_MODE_1 = 0x01, // �������õĴ���ͨ��ģʽ1��ASCIIģʽ
	SW_UART_MODE_2 = 0x02, // �������õĴ���ͨ��ģʽ2��͸��ģʽ

	HW_UART_MODE_0 = 0x80, // Ӳ���������õĴ���ͨ��ģʽ0��Э�鴫��ģʽ C0 = 1, C1 = 1
	HW_UART_MODE_1 = 0x81, // Ӳ���������õĴ���ͨ��ģʽ1��ASCIIģʽ C0 = 0, C1 = 1
	HW_UART_MODE_2 = 0x82, // Ӳ���������õĴ���ͨ��ģʽ2��͸��ģʽ C0 = 1, C1 = 0
};

/* ������ */
enum ERROR
{
	DEF_CMD_SUCCESS = 0x00,		// ����ִ�гɹ�
	DEF_CMD_ERR_TIMEOUT = 0xe1, // ���ڽ���һ���ֽڳ�ʱ
	DEF_CMD_ERR_HEAD = 0xe2,	// ���ڽ��հ�ͷ�ֽڳ���
	DEF_CMD_ERR_CMD = 0xe3,		// ���ڽ������������
	DEF_CMD_ERR_SUM = 0xe4,		// �ۼӺͼ���ֵ��ƥ��
	DEF_CMD_ERR_PARA = 0xe5,	// ��������
	DEF_CMD_ERR_OPERATE = 0xe6	// ֡������ִ��ʧ��
};

/* ���ݽṹ�� */
typedef struct buffer
{
	int len;
	char data[70];
} BUFFER;

/* ָ��֡�ṹ�� */
typedef struct uart_fmt
{
	char HEAD[2];  // ֡ͷ.�̶�Ϊ 0x57��0xAB
	char ADDR;	   // ��ַ�룺Ĭ��Ϊ 0x00��������ó� 0x01---0xFE����ֻ�ܽ��ն�Ӧ��ַ����ַ��Ϊ 0xFF ���������0xFF Ϊ�㲥����оƬ����Ҫ����Ӧ��
	char CMD;	   // �����룺��Ч��Χ��0x01---0x3F, ����Ӧ���ʱ��������Ϊ��ԭ������ | 0x80 �쳣Ӧ���ʱ��������Ϊ��ԭ������ | 0xC0
	uint8_t LEN;   // ���ݳ��ȣ��������������ݲ��֣�������֡ͷ�ֽڡ���ַ�롢��������ۼӺ��ֽ�
	char DATA[50]; // ���ݣ�ռ N ���ֽڣ�N ��Ч��ΧΪ 0---64
	char SUM;	   // �ۼӺͣ����㷽ʽΪ�� SUM = HEAD+ADDR+CMD+LEN+DATA
} UART_FMT;

/* Ӧ����ṹ�� */
struct RESULTS
{
	char cmd;
	ERROR error;
};

class Ch9329
{
private:
	typedef void (*callbackFunction)(void);	   // ����ָ��ģ��
	callbackFunction _USB_CONNECT_FUNC = NULL; // USB������ʱ�����ص�

	typedef void (*callbackFunction2)(char *buff, int length); // ����ָ��ģ��
	callbackFunction2 _GET_HID_REPORT_FUNC = NULL;			   // �Զ��� HID �豸���յ�����ʱ�����ص�

	typedef void (*callbackFunction3)(RESULTS e); // �޷���ֵ ����ָ��ģ��
	callbackFunction3 _GET_RESULTS_FUNC = NULL;	  // USB���ӻص�����ָ��

	void task(); // ���շ��ʹ�������
	static void startTaskImpl(void *_this) { static_cast<Ch9329 *>(_this)->task(); }
	void analyzeCommand(BUFFER buf); // ���ݷ�������

protected:
	QueueHandle_t queueMsg = xQueueCreate(5, sizeof(UART_FMT));			  // �������
	void sendCommand(char command, uint8_t length = 0, char *buf = NULL); // ��������

	bool getBit(char data, int position);					 // ��ȡ�ֽ�ָ��λ�õ�ֵ
	void setSum(UART_FMT *data);							 // ����У���
	void configTobuf(char *s);								 // ��оƬ������Ϣת��Ϊ����
	void bufToconfig(char s[]);								 // ������ת��Ϊ���ýṹ��
	void int32ToBaudrate(int32_t data, char *s, int offset); // �� int32_t ת��Ϊ������ byte ����
	int32_t BaudrateToint32(char *data);					 // �Ѳ����� byte ����ת��Ϊ int32_t
	BUFFER frameTObuf(UART_FMT data);						 // ��ָ��֡�ṹ��ת��Ϊ byte ����
	UART_FMT bufTOframe(BUFFER buf);						 // �� byte ����ת��Ϊָ��֡�ṹ��

public:
	uint16_t version; //оƬ�汾, ��8λ������������, ��8λ����С������

	bool num_lock_light;	// ���ּ��̵�  	0:off, 1:on
	bool caps_lock_light;	// ��Сд��		0:off, 1:on
	bool scroll_lock_light; // ������		0:off, 1:on

	// оƬ������Ϣ
	WORK_MODE work_mode = SW_WORK_MODE_0; // оƬ����ģʽ
	UART_MODE uart_mode = SW_UART_MODE_0; // ����ͨ��ģʽ
	int8_t addr = CHIP_ADDR;			  // оƬ����ͨ�ŵ�ַ��Ĭ��Ϊ 0x00
	int32_t baudrate = 9600;			  // ����ͨ�Ų����ʣ�Ĭ��Ϊ 0x00002580�����ֽ���ǰ����������Ϊ 9600bps
	int16_t packet_interval = 3;		  // ����ͨ�Ű������Ĭ��Ϊ 3ms����оƬ������� 3mS δ���յ���һ���ֽ����ʾ��������
	int32_t vid = 0x1A86;				  // VID Ĭ��оƬ VID Ϊ 0x1A86
	int32_t pid = 0xE129;				  // PID Ĭ��оƬ PID Ϊ 0xE129

	bool en_custom_str = false; // ʹ���Զ����ַ���������
	bool en_v_str = false;		// ʹ���Զ��峧���ַ���������
	bool en_p_str = false;		// ʹ���Զ����Ʒ�ַ���������
	bool en_s_str = false;		// ʹ���Զ������к��ַ���������

	String str_manufacturer; // ��������
	String str_product;		 // ��Ʒ����
	String str_serial;		 // ��Ʒ���к�

	// �������ý��� ASCII ģʽ��Ч
	int16_t kb_updata_interval = 0;										   // �����ϴ�ʱ����,	Ĭ��Ϊ 0ms����оƬ�ϴ���ǰ 1 ������֮�������ϴ���һ������
	int16_t kb_reless_interval = 1;										   // USB �����ͷ���ʱʱ��,	Ĭ��Ϊ 1ms����оƬ�ϴ��갴���������ݰ�֮�� 1ms ���ϴ������ͷ����ݰ�
	bool kb_autoEnter;													   // USB �����Զ��س���־,	0x00 ��ʾ���Զ����лس���0x01 ��ʾ�����������Զ����лس�
	char enter_1[4] = {0x0d, 0x00, 0x00, 0x00};							   // USB ���̻س���1,		Ĭ������ ASCII ֵΪ 0x0D ʱ���лس�
	char enter_2[4] = {0x00, 0x00, 0x00, 0x00};							   // USB ���̻س���2,		Ĭ������ ASCII ֵΪ 0x0D ʱ���лس�
	char filter_str[8] = {0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00}; // USB ���̹��˿�ʼ�������ַ���,	ǰ 4 ���ֽ�Ϊ���˿�ʼ�ַ����� 4���ֽ�Ϊ���˽����ַ�
	bool kb_fast_update;												   // USB ���̿����ϴ���־,	0x00 �����ٶȣ��ϴ��� 1 ���ַ������ͷŰ�����, 0x01 �����ϴ�ģʽ�������ַ��ϴ�����ٷ����ͷŰ�����

	Ch9329();
	void test_command(char command, char length = 0, ...); // ���Ͳ�������

	void reset();						// ����оƬ
	void factory_data_reset();			// �ָ�����������
	void get_info();					// ��ȡоƬ��Ϣ
	void get_usb_manufacturer_string(); // ��ȡ��������
	void get_usb_product_string();		// ��ȡ��Ʒ����
	void get_usb_serial_string();		// ��ȡ���к�����
	void get_configure();				// ��ȡ����

	void set_hid_report(char *buff, uint8_t length); // �����Զ��� HID ���ݰ�
	void set_usb_manufacturer_string(String str);	 // ���ó���������
	void set_usb_product_string(String str);		 // ���ò�Ʒ������
	void set_usb_serial_string(String str);			 // �������к�������

	void set_configure(WORK_MODE work_mode = SW_WORK_MODE_0, // ����оƬ����
					   UART_MODE uart_mode = SW_UART_MODE_0,
					   int32_t baudrate = 9600,
					   int32_t vid = 0x1A86,
					   int32_t pid = 0xE129);
	void set_configure(int32_t baudrate, int32_t vid, int32_t pid) { set_configure(SW_WORK_MODE_0, SW_UART_MODE_0, baudrate, vid, pid); }
	void set_configure(int32_t vid, int32_t pid) { set_configure(SW_WORK_MODE_0, SW_UART_MODE_0, 9600, vid, pid); }

	void attach_usb_connect(callbackFunction newFunction) { _USB_CONNECT_FUNC = newFunction; }		  // ע�� usb ���ӻص�����
	void attach_get_hid_report(callbackFunction2 newFunction) { _GET_HID_REPORT_FUNC = newFunction; } // ע�� ��״̬�ı�ص�����
	void attach_get_results(callbackFunction3 newFunction) { _GET_RESULTS_FUNC = newFunction; }		  // ע�� ��״̬�ı�ص�����
};

#endif
