#pragma once

#include "Arduino.h"

#define ISMODIFYKEY(val) val == KEY_WIN || val == KEY_ALT || val == KEY_SHIFT || val == KEY_CTRL
#define ISMOUSEKEY(val) val == MOUSE_BUTTON_MIDDLE || val == MOUSE_BUTTON_RIGHT || val == MOUSE_BUTTON_LEFT

typedef byte ModifyKey;
typedef byte KeyboardKey;

// 此部分键值魔改过, 更方便代码写法, 偷懒吧
const KeyboardKey END = 0x00;
const KeyboardKey KEY_WIN = 0x80;
const KeyboardKey KEY_ALT = 0x40;
const KeyboardKey KEY_SHIFT = 0x02;
const KeyboardKey KEY_CTRL = 0x01;
//  const KeyboardKey KEY_RIGHT_WIN = 0x08;
//  const KeyboardKey KEY_RIGHT_ALT = 0x04;
//  const KeyboardKey KEY_LEFT_SHIFT = 0x20;
//  const KeyboardKey KEY_LEFT_CTRL = 0x10;

// 左侧功能键
const KeyboardKey KEY_TAB = 0x2B;
const KeyboardKey KEY_CAPS_LOCK = 0x39;
const KeyboardKey KEY_SPACEBAR = 0x2C;
// 右侧功能键
const KeyboardKey KEY_BACKSPACE = 0x2A;
const KeyboardKey KEY_ENTER = 0x58;
const KeyboardKey KEY_APP = 0x65;
// 没啥用三人组
const KeyboardKey KEY_SCROLL_LOCK = 0x47;
const KeyboardKey KEY_PRINT_SCREEN = 0x46;
const KeyboardKey KEY_PAUSE = 0x48;
// 翻页区
const KeyboardKey KEY_INSERT = 0x49;
const KeyboardKey KEY_DELETE = 0x4C;
const KeyboardKey KEY_HOME = 0x4A;
const KeyboardKey KEY_END = 0x4D;
const KeyboardKey KEY_PAGE_UP = 0x4B;
const KeyboardKey KEY_PAGE_DOWN = 0x4E;
// 方向键
const KeyboardKey KEY_UP_ARROW = 0x52;
const KeyboardKey KEY_DOWN_ARROW = 0x51;
const KeyboardKey KEY_LEFT_ARROW = 0x50;
const KeyboardKey KEY_RIGHT_ARROW = 0x4F;
// FN功能区
const KeyboardKey KEY_ESC = 0x29;
const KeyboardKey KEY_F1 = 0x3A;
const KeyboardKey KEY_F2 = 0x3B;
const KeyboardKey KEY_F3 = 0x3C;
const KeyboardKey KEY_F4 = 0x3D;
const KeyboardKey KEY_F5 = 0x3E;
const KeyboardKey KEY_F6 = 0x3F;
const KeyboardKey KEY_F7 = 0x40;
const KeyboardKey KEY_F8 = 0x41;
const KeyboardKey KEY_F9 = 0x42;
const KeyboardKey KEY_F10 = 0x43;
const KeyboardKey KEY_F11 = 0x44;
const KeyboardKey KEY_F12 = 0x45;
// 数字区
const KeyboardKey KEY_TILDE = 0x35;
const KeyboardKey KEY_1 = 0x1E;
const KeyboardKey KEY_2 = 0x1F;
const KeyboardKey KEY_3 = 0x20;
const KeyboardKey KEY_4 = 0x21;
const KeyboardKey KEY_5 = 0x22;
const KeyboardKey KEY_6 = 0x23;
const KeyboardKey KEY_7 = 0x24;
const KeyboardKey KEY_8 = 0x25;
const KeyboardKey KEY_9 = 0x26;
const KeyboardKey KEY_0 = 0x27;
const KeyboardKey KEY_SUB = 0x2D;
const KeyboardKey KEY_ADD = 0x2E;
// 字母区
const KeyboardKey KEY_A = 0x04;
const KeyboardKey KEY_B = 0x05;
const KeyboardKey KEY_C = 0x06;
const KeyboardKey KEY_D = 0x07;
const KeyboardKey KEY_E = 0x08;
const KeyboardKey KEY_F = 0x09;
const KeyboardKey KEY_G = 0x0A;
const KeyboardKey KEY_H = 0x0B;
const KeyboardKey KEY_I = 0x0C;
const KeyboardKey KEY_J = 0x0D;
const KeyboardKey KEY_K = 0x0E;
const KeyboardKey KEY_L = 0x0F;
const KeyboardKey KEY_M = 0x10;
const KeyboardKey KEY_N = 0x11;
const KeyboardKey KEY_O = 0x12;
const KeyboardKey KEY_P = 0x13;
const KeyboardKey KEY_Q = 0x14;
const KeyboardKey KEY_R = 0x15;
const KeyboardKey KEY_S = 0x16;
const KeyboardKey KEY_T = 0x17;
const KeyboardKey KEY_U = 0x18;
const KeyboardKey KEY_V = 0x19;
const KeyboardKey KEY_W = 0x1A;
const KeyboardKey KEY_X = 0x1B;
const KeyboardKey KEY_Y = 0x1C;
const KeyboardKey KEY_Z = 0x1D;
// 字母区旁边的特殊字符
const KeyboardKey KEY_BACKSLASH = 0x31;            // 反斜杠
const KeyboardKey KEY_LEFT_CURLY_BRACKETS = 0x2F;  // [
const KeyboardKey KEY_RIGHT_CURLY_BRACKETS = 0x30; // ]
const KeyboardKey KEY_COMMA = 0x36;                // ,
const KeyboardKey KEY_PERD = 0x37;                 // .
const KeyboardKey KEY_SLASH = 0x38;                // /
const KeyboardKey KEY_COLON = 0x33;                // ;
const KeyboardKey KEY_QUOTATION = 0x34;            // '
// 小键盘区
const KeyboardKey KEY_NUMBER_1 = 0x59;
const KeyboardKey KEY_NUMBER_2 = 0x5A;
const KeyboardKey KEY_NUMBER_3 = 0x5B;
const KeyboardKey KEY_NUMBER_4 = 0x5C;
const KeyboardKey KEY_NUMBER_5 = 0x5D;
const KeyboardKey KEY_NUMBER_6 = 0x5E;
const KeyboardKey KEY_NUMBER_7 = 0x5F;
const KeyboardKey KEY_NUMBER_8 = 0x60;
const KeyboardKey KEY_NUMBER_9 = 0x61;
const KeyboardKey KEY_NUMBER_0 = 0x62;
const KeyboardKey KEY_NUMBER_LOCK = 0x53;
const KeyboardKey KEY_NUMBER_DIV = 0x54;
const KeyboardKey KEY_NUMBER_MUL = 0x55;
const KeyboardKey KEY_NUMBER_SUB = 0x56;
const KeyboardKey KEY_NUMBER_ADD = 0x57;
const KeyboardKey KEY_NUMBER_POINT = 0x63;
const KeyboardKey KEY_NUMBER_ENTER = 0x28;

// 电源管理区
// 对于ACPI键，共2个字节，第1个字节为REPORT ID，固定为0x01，第2个字节为ACPI键码。
// bit 2: wakeup   bit 1: sleep    bit 0: Power     1:按下 0: 释放
typedef char ACPI[2];
const ACPI KEY_POWER = {0x01, 0x01};
const ACPI KEY_SLEEP = {0x01, 0x02};
const ACPI KEY_WAKEUP = {0x01, 0x04};

// 多媒体按键及对应的键码表
// 1:按下 0: 释放
typedef char MediaKey[4];
const MediaKey KEY_MEDIA_EJECT = {0x02, 0x80, 0x00, 0x00};         // 弹出光盘(没试过)
const MediaKey KEY_MEDIA_CD_STOP = {0x02, 0x40, 0x00, 0x00};       // CD停止(没试过)
const MediaKey KEY_MEDIA_PREV_TRACK = {0x02, 0x20, 0x00, 0x00};    // 上一曲
const MediaKey KEY_MEDIA_NEXT_TRACK = {0x02, 0x10, 0x00, 0x00};    // 下一曲
const MediaKey KEY_MEDIA_PLAY_PAUSE = {0x02, 0x08, 0x00, 0x00};    // 暂停播放
const MediaKey KEY_MEDIA_MUTE = {0x02, 0x04, 0x00, 0x00};          // 静音
const MediaKey KEY_MEDIA_VOLUME_DOWN = {0x02, 0x02, 0x00, 0x00};   // 音量减
const MediaKey KEY_MEDIA_VOLUME_UP = {0x02, 0x01, 0x00, 0x00};     // 音量加
const MediaKey KEY_MEDIA_REFRESH = {0x02, 0x00, 0x80, 0x00};       // 刷新
const MediaKey KEY_MEDIA_WWW_STOP = {0x02, 0x00, 0x40, 0x00};      // 停止播放
const MediaKey KEY_MEDIA_WWW_FORWARD = {0x02, 0x00, 0x20, 0x00};   // 浏览器前进
const MediaKey KEY_MEDIA_WWW_BACK = {0x02, 0x00, 0x10, 0x00};      // 浏览器后退
const MediaKey KEY_MEDIA_WWW_HOME = {0x02, 0x00, 0x08, 0x00};      // 浏览器主页
const MediaKey KEY_MEDIA_WWW_FAVORITES = {0x02, 0x00, 0x04, 0x00}; // 浏览器收藏键(不生效)
const MediaKey KEY_MEDIA_WWW_SEARCH = {0x02, 0x00, 0x02, 0x00};    // 浏览器搜索框
const MediaKey KEY_MEDIA_EMAIL = {0x02, 0x00, 0x01, 0x00};         // 打开邮箱
const MediaKey KEY_MEDIA_REWIND = {0x02, 0x00, 0x00, 0x80};        // 倒带(不生效)
const MediaKey KEY_MEDIA_RECORD = {0x02, 0x00, 0x00, 0x40};        // 录音(不生效)
const MediaKey KEY_MEDIA_MINIMIZE = {0x02, 0x00, 0x00, 0x20};      // 最小化(不生效)
const MediaKey KEY_MEDIA_MY_COMPUTER = {0x02, 0x00, 0x00, 0x10};   // 打开我的电脑
const MediaKey KEY_MEDIA_SCREEN_SAVE = {0x02, 0x00, 0x00, 0x08};   // 截屏(不生效)
const MediaKey KEY_MEDIA_CALCULATOR = {0x02, 0x00, 0x00, 0x04};    // 打开计算器
const MediaKey KEY_MEDIA_EXPLORER = {0x02, 0x00, 0x00, 0x02};      // 打开浏览器(不生效)
const MediaKey KEY_MEDIA_MEDIA = {0x02, 0x00, 0x00, 0x01};         // 打开音乐播放器

// 鼠标按键,为了与键盘指令分开, 这边用16位表示,  并且高位是FF
typedef int16_t MouseKey;
const MouseKey MOUSE_BUTTON_MIDDLE = 0xFF04;
const MouseKey MOUSE_BUTTON_RIGHT = 0xFF02;
const MouseKey MOUSE_BUTTON_LEFT = 0xFF01;

#define ASCIIOFFSET 32 // ASCII 码表偏移量
// 为了区别大小写,和打印一些特殊字符, 用 16位表示, 高8位表示 modify key,即辅助键键值, 0x02表示同时按下shift键
const int16_t HIDCONVERTASCII[95] = {
    0x002C, // (Space)
    0x021E, // !
    0x0234, // "
    0x0220, // #
    0x0221, // $
    0x0222, // %
    0x0224, // &
    0x0034, // '
    0x0226, // (
    0x0227, // )
    0x0055, // *
    0x0057, // +
    0x0036, // ,
    0x002D, // -
    0x0037, // .
    0x0038, // /
    0x0027, // 0
    0x001E, // 1
    0x001F, // 2
    0x0020, // 3
    0x0021, // 4
    0x0022, // 5
    0x0023, // 6
    0x0024, // 7
    0x0025, // 8
    0x0026, // 9
    0x0233, // :
    0x0033, // ;
    0x0236, // <
    0x022E, // =
    0x0237, // >
    0x0238, // ?
    0x021F, // @
    0x0204, // A
    0x0205, // B
    0x0206, // C
    0x0207, // D
    0x0208, // E
    0x0209, // F
    0x020A, // G
    0x020B, // H
    0x020C, // I
    0x020D, // J
    0x020E, // K
    0x020F, // L
    0x0210, // M
    0x0211, // N
    0x0212, // O
    0x0213, // P
    0x0214, // Q
    0x0215, // R
    0x0216, // S
    0x0217, // T
    0x0218, // U
    0x0219, // V
    0x021A, // W
    0x021B, // X
    0x021C, // Y
    0x021D, // Z
    0x002F, // [
    0x0031, // 反斜杠
    0x0030, // ]
    0x0223, // ^
    0x022D, // _
    0x0035, // `
    0x0004, // a
    0x0005, // b
    0x0006, // c
    0x0007, // d
    0x0008, // e
    0x0009, // f
    0x000A, // g
    0x000B, // h
    0x000C, // i
    0x000D, // j
    0x000E, // k
    0x000F, // l
    0x0010, // m
    0x0011, // n
    0x0012, // o
    0x0013, // p
    0x0014, // q
    0x0015, // r
    0x0016, // s
    0x0017, // t
    0x0018, // u
    0x0019, // v
    0x001A, // w
    0x001B, // x
    0x001C, // y
    0x001D, // z
    0x022F, // {
    0x0231, // |
    0x0230, // }
    0x0235  // ~
};

// https://blog.csdn.net/bona020/article/details/101289147?spm=1001.2101.3001.6661.1&utm_medium=distribute.pc_relevant_t0.none-task-blog-2%7Edefault%7ECTRLIST%7ERate-1-101289147-blog-117631823.pc_relevant_default&depth_1-utm_source=distribute.pc_relevant_t0.none-task-blog-2%7Edefault%7ECTRLIST%7ERate-1-101289147-blog-117631823.pc_relevant_default&utm_relevant_index=1