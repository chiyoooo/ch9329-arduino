#include "Ch9329.h"

Ch9329 ch9329;

// 收到 HID 数据包后打印内容
void print_usb_report(char *buf, int len)
{
    Serial.write(buf, len);
    Serial.println(" ");
}

void setup()
{
    //打开串口
    Serial.begin(115200);

    ch9329.attach_get_hid_report(print_usb_report);

    pinMode(19, INPUT);
}

void loop()
{
    if (digitalRead(19) == 0)
    {
        static int n = 0;
        BUFFER buf;
        buf.len = 1;
        buf.data[0] = n++;
        ch9329.set_hid_report(buf.data, buf.len);
    }
}
