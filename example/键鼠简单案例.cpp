#include <keymouser.h>
#include "OneButton.h"

Keymouser ch9329;
OneButton btn1(19, true);

void sayHello()
{
    ch9329.click(KEY_WIN, END);
    delay(200);
    ch9329.printstr("text");
    delay(200);
    ch9329.click(KEY_ENTER, END);
    delay(2000);
    ch9329.printstr("Hello world!");
}

void btn1_click_cb(void)
{
    ch9329.click(KEY_MEDIA_MUTE);
}
void btn1_press_start_cb(void)
{
    ch9329.press(KEY_SHIFT, END);
    ch9329.start_drag(MOUSE_BUTTON_MIDDLE, 2, 0);
}
void btn1_press_cb(void)
{
    ch9329.press(KEY_SHIFT, END);
    ch9329.start_drag(MOUSE_BUTTON_MIDDLE, 2, 0);
}

void btn1_press_stop_cb(void)
{
    ch9329.release(KEY_SHIFT, END);
    ch9329.stop_drag(MOUSE_BUTTON_MIDDLE);
}

void setup()
{
    sayHello();

    btn1.attachClick(btn1_click_cb);
    btn1.attachLongPressStart(btn1_press_start_cb);
    btn1.attachDuringLongPress(btn1_press_cb);
    btn1.attachLongPressStop(btn1_press_stop_cb);
}

void loop()
{
    btn1.tick();
}
#include <keymouser.h>
#include "OneButton.h"

Keymouser ch9329;
OneButton btn1(19, true);

void sayHello()
{
    ch9329.click(KEY_WIN, END);
    delay(200);
    ch9329.printstr("text");
    delay(200);
    ch9329.click(KEY_ENTER, END);
    delay(2000);
    ch9329.printstr("Hello world!");
}

void btn1_click_cb(void)
{
    ch9329.click(KEY_MEDIA_MUTE);
}
void btn1_press_start_cb(void)
{
    ch9329.press(KEY_SHIFT, END);
    ch9329.start_drag(MOUSE_BUTTON_MIDDLE, 2, 0);
}
void btn1_press_cb(void)
{
    ch9329.press(KEY_SHIFT, END);
    ch9329.start_drag(MOUSE_BUTTON_MIDDLE, 2, 0);
}

void btn1_press_stop_cb(void)
{
    ch9329.release(KEY_SHIFT, END);
    ch9329.stop_drag(MOUSE_BUTTON_MIDDLE);
}

void setup()
{
    sayHello();

    btn1.attachClick(btn1_click_cb);
    btn1.attachLongPressStart(btn1_press_start_cb);
    btn1.attachDuringLongPress(btn1_press_cb);
    btn1.attachLongPressStop(btn1_press_stop_cb);
}

void loop()
{
    btn1.tick();
}
