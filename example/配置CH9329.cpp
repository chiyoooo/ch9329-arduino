#include "Ch9329.h"
#include "Arduino.h"

Ch9329 ch9329;

// USB 链接
void usb_connect_cb()
{
    Serial.println("success to connect PC");
    Serial.printf("num_lock[%d] caps_lock[%d] scroll_lock[%d] \n",
                  ch9329.num_lock_light, ch9329.caps_lock_light, ch9329.scroll_lock_light);
}

// 异常应答包触发回调
void get_result_cb(RESULTS res) { Serial.printf("CMD: 0x%02x  Results: 0x%02x \n", res.cmd, res.error); }

void setup()
{
    //打开串口
    Serial.begin(115200);

    ch9329.attach_usb_connect(usb_connect_cb); // 注册 USB 链接回调
    ch9329.attach_get_results(get_result_cb);  // 注册 异常应答包触发回调

    ch9329.set_usb_manufacturer_string("WCH");
    delay(200);
    ch9329.set_usb_product_string("MyCH9329");
    delay(200);
    ch9329.set_usb_serial_string("20221020");
    delay(200);
    ch9329.set_configure(0x1234, 0x5678);

    pinMode(19, INPUT);
}

void loop()
{
    if (digitalRead(19) == 0)
    {
        ch9329.get_info(); // 获取芯片信息
        delay(100);
        ch9329.get_configure(); // 获取芯片配置
        delay(100);
        ch9329.get_usb_manufacturer_string(); // 获取厂商描述
        delay(100);
        ch9329.get_usb_product_string(); // 获取产品描述
        delay(100);
        ch9329.get_usb_serial_string(); // 获取序列号描述
    }
}
